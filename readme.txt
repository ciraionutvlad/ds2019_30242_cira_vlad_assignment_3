**Project Setup (after clone)**

1. Clone the project to your computer
2. Import the project in InteliJ (or your IDE)
3. Create a database schema "spring-demo" in MySQL 
4. Change the line 26 from application.properties file located in /src/main/resources to allow table creation in the DB: spring.jpa.hibernate.ddl-auto = create
5. Run the application
6. Check is the person table was created
7. Change the line 26 from application.properties file located in /src/main/resources to allow table creation in the DB: spring.jpa.hibernate.ddl-auto = validate

**Running Server Aplication**
1. Select Build - Build application
2. Select Run - Run ”Application”
3. Open a web browser
4. Acces localhost:8080

**Running Client Aplication**
1. Run in terminal the command: mvn spring-boot:run
2. Acces url: localhost:8083