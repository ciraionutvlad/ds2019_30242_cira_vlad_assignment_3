package com.ds.onlinemedication;

import com.ds.onlinemedication.dto.MedicationPlanDTO;
import com.ds.onlinemedication.service.MedicationPlanService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Collections;
import java.util.List;

@SpringBootApplication
public class PillDispenserApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(PillDispenserApplication.class);
        app.setDefaultProperties(Collections
                .singletonMap("server.port", "8083"));
        app.run(args);
    }


    /*@Bean
    RmiProxyFactoryBean rmiProxy() {
        RmiProxyFactoryBean bean = new RmiProxyFactoryBean();
        bean.setServiceInterface(MedicationPlanService.class);
        bean.setServiceUrl("rmi://localhost:2099/PillDispenser");

        return bean;
    }*/
//
//    public static void main(String[] args) {
//        /*MedicationPlanService medicationPlanBean =
//                new AnnotationConfigApplicationContext(PillDispenserApplication.class).getBean(MedicationPlanService.class);
//        try {
//            medicationPlanBean.findAll();
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }*/
////        try {
////            MedicationPlanService medicationPlanService =
////                    (MedicationPlanService) Naming.lookup("rmi://localhost:2099/PillDispenser");
////            medicationPlanService.findAll();
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//
//
//        /*SpringApplication app = new SpringApplication(PillDispenserApplication.class);
//        app.setDefaultProperties(Collections
//                .singletonMap("server.port", "8083"));
//        app.run(args);*/
//
//
//
//        // System.getProperties().put( "server.port", 9080 );
//        //new SpringApplication(PillDispenserApplication.class);
//
//        try {
//            Registry registry = LocateRegistry.getRegistry(1099);
//            MedicationPlanService server = (MedicationPlanService) registry.lookup("PillDispenser");
//
//            List<MedicationPlanDTO> response = server.findAll();
//
//            System.out.println("response: " + response);
//        } catch (Exception e) {
//            System.err.println("Client exception: " + e.toString());
//            e.printStackTrace();
//        }
//    }

}
