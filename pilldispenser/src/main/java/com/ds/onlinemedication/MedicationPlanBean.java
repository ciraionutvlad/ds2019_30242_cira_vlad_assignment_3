package com.ds.onlinemedication;

import com.ds.onlinemedication.service.MedicationPlanService;
import org.springframework.beans.factory.annotation.Autowired;

import java.rmi.RemoteException;

public class MedicationPlanBean {

    @Autowired
    private MedicationPlanService medicationPlanService;

    public void listMedicationPlans() throws RemoteException {
        System.out.println("-- list medication plans --");
        medicationPlanService.findAll();
    }
}