package com.ds.onlinemedication.dto;


import com.ds.onlinemedication.entities.Medication;

public class MedicationBuilder {
    public static Medication generateMedication(MedicationDTO medicationDTO) {
        return new Medication(medicationDTO.getId(), medicationDTO.getName(), medicationDTO.getDosage(),
                medicationDTO.getListOfSideEffects());
    }

    public static MedicationDTO generateMedicationDTO(Medication medication) {
        return new MedicationDTO(medication.getId(), medication.getName(), medication.getDosage(),
                medication.getListOfSideEffects());
    }
}
