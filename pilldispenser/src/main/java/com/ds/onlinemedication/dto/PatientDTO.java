package com.ds.onlinemedication.dto;

import java.io.Serializable;

public class PatientDTO implements Serializable {
    private Integer id;
    private String name;
    private String birthDate;
    private String gender;
    private String address;
    private String medicalRecord;
    private Integer caregiverId;
    private CaregiverDTO caregiver;

    public PatientDTO(Integer id, String name, String birthDate, String gender, String address, String medicalRecord, Integer caregiverId) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.caregiverId = caregiverId;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCaregiverId() {
        return caregiverId;
    }

    public void setCaregiverId(Integer caregiverId) {
        this.caregiverId = caregiverId;
    }

    public void setCaregiver(CaregiverDTO caregiver) {
        this.caregiver = caregiver;
    }

    public CaregiverDTO getCaregiver() {
        return caregiver;
    }

    @Override
    public String toString() {
        return "PatientDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", gender='" + gender + '\'' +
                ", address='" + address + '\'' +
                ", medicalRecord='" + medicalRecord + '\'' +
                '}';
    }
}
