package com.ds.onlinemedication.dto;


import com.ds.onlinemedication.entities.Doctor;

public class DoctorBuilder {

    public static Doctor generateDoctor(DoctorDTO doctorDTO) {
        return new Doctor(doctorDTO.getId(), doctorDTO.getName(), doctorDTO.getBirthDate(),
                doctorDTO.getGender(), doctorDTO.getPhone(), doctorDTO.getAddress());
    }

    public static DoctorDTO generateDoctorDTO(Doctor doctor) {
        return new DoctorDTO(doctor.getId(), doctor.getName(), doctor.getBirthDate(),
                doctor.getGender(), doctor.getPhone(), doctor.getAddress());
    }
}
