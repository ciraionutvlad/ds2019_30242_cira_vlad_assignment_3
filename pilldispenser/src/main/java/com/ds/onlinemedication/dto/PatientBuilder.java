package com.ds.onlinemedication.dto;


import com.ds.onlinemedication.entities.Patient;

public class PatientBuilder {
    public static Patient generatePatient(PatientDTO patientDTO) {
        return new Patient(patientDTO.getId(), patientDTO.getName(), patientDTO.getBirthDate(),
                patientDTO.getGender(), patientDTO.getAddress(), patientDTO.getMedicalRecord(),
                CaregiverBuilder.generateCaregiver(patientDTO.getCaregiver()));
    }

    public static PatientDTO generatePatientDTO(Patient patient) {
        return new PatientDTO(patient.getId(), patient.getName(), patient.getBirthDate(),
                patient.getGender(), patient.getAddress(), patient.getMedicalRecord(),
                CaregiverBuilder.generateCaregiverDTO(patient.getCaregiver()).getId());
    }
}
