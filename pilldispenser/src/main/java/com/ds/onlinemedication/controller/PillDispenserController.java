package com.ds.onlinemedication.controller;

import com.ds.onlinemedication.dto.MedicationPlanDTO;
import com.ds.onlinemedication.service.MedicationPlanService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

@Controller
@RequestMapping(value = "/")
public class PillDispenserController {

    @GetMapping(value = "")
    public String index() {
        return "index";
    }

    @GetMapping("medication_plan")
    public String test(@RequestParam(value = "id") String id, Model model) {
        try {
            Registry registry = LocateRegistry.getRegistry(1099);
            MedicationPlanService server = (MedicationPlanService) registry.lookup("PillDispenser");

            List<MedicationPlanDTO> response = server.findAllById(Integer.parseInt(id));
            model.addAttribute("medicationPlans", response);

            System.out.println("response: " + response);
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
        return "view_medication_plans";
    }


//    @GetMapping("test")
//    public String ping(@RequestParam(value = "id") String id) {
//        System.out.println("test");
//        return "view_medication_plans";
//    }

}
