package com.ds.onlinemedication.entities;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication")
public class Medication {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "dosage")
    private String dosage;

    @Column(name = "list_of_side_effects")
    private String listOfSideEffects;

    @ManyToMany(mappedBy = "medications", fetch = FetchType.LAZY)
    Set<MedicationPlan> medicationPlans;

    public Medication(Integer id,String name, String dosage, String listOfSideEffects) {
        this.id = id;
        this.name = name;
        this.dosage = dosage;
        this.listOfSideEffects = listOfSideEffects;
    }

    public Medication() {
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDosage() {
        return dosage;
    }

    public String getListOfSideEffects() {
        return listOfSideEffects;
    }

}
