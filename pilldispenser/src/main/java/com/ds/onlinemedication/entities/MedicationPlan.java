package com.ds.onlinemedication.entities;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication_plan")
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "diagnostic")
    private String diagnostic;

    @Column(name = "intake_intervals")
    private String intakeIntervals;

    @Column(name = "period_of_the_treatment")
    private String periodOfTheTreatment;

    @Column(name = "list_of_medication")
    private String listOfMedication;

    @ManyToOne//(fetch = FetchType.LAZY)
    private Doctor doctor;

    @ManyToOne//(fetch = FetchType.LAZY)
    private Patient patient;

    @ManyToMany(
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(
            name = "medication_has_medication_plan",
            joinColumns = @JoinColumn(name = "medication_plan_id"),
            inverseJoinColumns = @JoinColumn(name = "medication_id"))
    Set<Medication> medications;

    public MedicationPlan(){

    }

    public MedicationPlan(Integer id, String diagnostic, String intakeIntervals, String periodOfTheTreatment, String listOfMedication) {
        this.id = id;
        this.diagnostic = diagnostic;
        this.intakeIntervals = intakeIntervals;
        this.periodOfTheTreatment = periodOfTheTreatment;
        this.listOfMedication = listOfMedication;
    }

    public Integer getId() {
        return id;
    }

    public String getDiagnostic() {
        return diagnostic;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public String getPeriodOfTheTreatment() {
        return periodOfTheTreatment;
    }

    public String getListOfMedication() {
        return listOfMedication;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public Patient getPatient() {
        return patient;
    }
}
