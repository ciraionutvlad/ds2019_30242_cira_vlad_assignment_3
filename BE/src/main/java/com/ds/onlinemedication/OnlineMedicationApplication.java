package com.ds.onlinemedication;

import com.ds.onlinemedication.dto.MedicationDTO;
import com.ds.onlinemedication.dto.MedicationPlanDTO;
import com.ds.onlinemedication.producer.Producer;
import com.ds.onlinemedication.service.MedicationPlanServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.List;

@SpringBootApplication
public class OnlineMedicationApplication{

    @Autowired
    Producer producer;

    public static void main(String[] args) {
       /* SpringApplication.run(OnlineMedicationApplication.class, args);
        FileReader reader = new FileReader();
        reader.test();*/

        ConfigurableApplicationContext ctx = SpringApplication.run(OnlineMedicationApplication.class, args);
        //List<MedicationPlanDTO> list = ctx.getBean(MedicationPlanServiceImpl.class).findAllById(2);
       // System.out.println("here" + list);
        //ctx.close();

//        try {
//            Registry registry = LocateRegistry.createRegistry(2099);
//            registry.rebind("PillDispenser", new OnlineMedicationApplication());
//
//            SpringApplication application = new SpringApplication(OnlineMedicationApplication.class);
//            application.setDefaultProperties(Collections.singletonMap("server.port", 8080));
//            application.run(args);
//
//
//           // registry.rebind("path/to/service_as_registry_key/CustomerService", new CustomerServiceImpl());
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }

        /*try {
            MedicationPlanService stub =
                    (MedicationPlanService) UnicastRemoteObject.exportObject(new MedicationPlanServiceImpl(), 0);

            Registry registry = LocateRegistry.createRegistry(2099);
            registry.rebind("PillDispenser", stub);

            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }*/
    }

    public void send() {
        producer.produceMessage();
    }

}
