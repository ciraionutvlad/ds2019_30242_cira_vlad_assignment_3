package com.ds.onlinemedication.controller;

import com.ds.onlinemedication.dto.CaregiverDTO;
import com.ds.onlinemedication.service.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(value = "/")
public class CaregiverController {

    private CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping("add-caregiver")
    public String showAddCaregiver(CaregiverDTO caregiverDTO) {
        return "add-caregiver";
    }

    @GetMapping("caregiver/edit/{id}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        CaregiverDTO caregiverDTO = caregiverService.findById(id);
        model.addAttribute("caregiver", caregiverDTO);
        return "update-caregiver";
    }

    @GetMapping(value = "caregiver/{id}")
    public CaregiverDTO findById(@PathVariable("id") Integer id) {
        return caregiverService.findById(id);
    }

    @GetMapping(value = "caregiver/all")
    public String findAll(Model model) {
        List<CaregiverDTO> list = caregiverService.findAll();
        model.addAttribute("caregivers", list);
        return "list-caregiver";
    }

    @PostMapping(value = "caregiver/add")
    public String insert(@Valid CaregiverDTO caregiverDTO, Model model) {
        caregiverService.insert(caregiverDTO);
        model.addAttribute("caregivers", caregiverService.findAll());
        return "list-caregiver";
    }

    @PostMapping(value = "caregiver/update/{id}")
    public String update(@PathVariable("id") int id, @Valid CaregiverDTO caregiverDTO, Model model) {
        caregiverDTO.setId(id);
        caregiverService.update(caregiverDTO);
        model.addAttribute("caregivers", caregiverService.findAll());
        return "list-caregiver";
    }

    @GetMapping(value = "caregiver/delete/{id}")
    public String delete(@PathVariable("id") Integer id, Model model) {
        caregiverService.delete(id);
        model.addAttribute("caregivers", caregiverService.findAll());
        return "list-caregiver";
    }
}
