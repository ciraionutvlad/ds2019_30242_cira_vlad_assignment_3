package com.ds.onlinemedication.controller;

import com.ds.onlinemedication.dto.DoctorDTO;
import com.ds.onlinemedication.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(value = "/")
public class DoctorController {

    private DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping("add-doctor")
    public String showAddDoctor(DoctorDTO doctorDTO) {
        return "add-doctor";
    }

    @GetMapping("doctor/edit/{id}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        DoctorDTO doctorDTO = doctorService.findById(id);
        model.addAttribute("doctor", doctorDTO);
        return "update-doctor";
    }

    @GetMapping(value = "doctor/{id}")
    public DoctorDTO findById(@PathVariable("id") Integer id) {
        return doctorService.findById(id);
    }

    @GetMapping(value = "doctor/all")
    public String findAll(Model model) {
        List<DoctorDTO> list = doctorService.findAll();
        model.addAttribute("doctors", list);
        return "list-doctor";
    }

    @PostMapping(value = "doctor/add")
    public String insert(@Valid DoctorDTO doctorDTO, Model model) {
        doctorService.insert(doctorDTO);
        model.addAttribute("doctors", doctorService.findAll());
        return "list-doctor";
    }

    @PostMapping(value = "doctor/update/{id}")
    public String update(@PathVariable("id") int id, @Valid DoctorDTO doctorDTO, Model model) {
        doctorDTO.setId(id);
        doctorService.update(doctorDTO);
        model.addAttribute("doctors", doctorService.findAll());
        return "list-doctor";
    }

    @GetMapping(value = "doctor/delete/{id}")
    public String delete(@PathVariable("id") Integer id, Model model) {
        doctorService.delete(id);
        model.addAttribute("doctors", doctorService.findAll());
        return "list-doctor";
    }

}
