package com.ds.onlinemedication.dto;

import com.ds.onlinemedication.entities.MedicationPlan;
import org.springframework.transaction.annotation.Transactional;

public class MedicationPlanBuilder {
    public static MedicationPlan generateMedicationPlan(MedicationPlanDTO medicationPlanDTO) {
        return new MedicationPlan(medicationPlanDTO.getId(), medicationPlanDTO.getDiagnostic(),
                medicationPlanDTO.getIntakeIntervals(), medicationPlanDTO.getPeriodOfTheTreatment(),
                medicationPlanDTO.getListOfMedication());
    }

    public static MedicationPlanDTO generateMedicationPlanDTO(MedicationPlan medicationPlan) {
        return new MedicationPlanDTO(medicationPlan.getId(), medicationPlan.getDiagnostic(),
                medicationPlan.getIntakeIntervals(), medicationPlan.getPeriodOfTheTreatment(),
                medicationPlan.getListOfMedication(), DoctorBuilder.generateDoctorDTO(medicationPlan.getDoctor()),
                PatientBuilder.generatePatientDTO(medicationPlan.getPatient()));
    }
}
