package com.ds.onlinemedication.dto;

import com.ds.onlinemedication.entities.Caregiver;

public class CaregiverBuilder {
    public static Caregiver generateCaregiver(CaregiverDTO caregiverDTO) {
        return new Caregiver(caregiverDTO.getId(), caregiverDTO.getName(), caregiverDTO.getBirthDate(),
                caregiverDTO.getGender(),caregiverDTO.getAddress(),
                caregiverDTO.getListOfPatientsTakenCareOf());
    }

    public static CaregiverDTO generateCaregiverDTO(Caregiver caregiver) {
        return new CaregiverDTO(caregiver.getId(), caregiver.getName(), caregiver.getBirthDate(),
                caregiver.getGender(), caregiver.getAddress(), caregiver.getListOfPatientsTakenCareOf());
    }
}
