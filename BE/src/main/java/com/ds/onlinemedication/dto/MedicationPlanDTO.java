package com.ds.onlinemedication.dto;

import java.io.Serializable;

public class MedicationPlanDTO implements Serializable {
    private Integer id;
    private String diagnostic;
    private String intakeIntervals;
    private String periodOfTheTreatment;
    private String listOfMedication;
    private DoctorDTO doctor;
    private PatientDTO patient;

    public MedicationPlanDTO(Integer id, String diagnostic, String intakeIntervals, String periodOfTheTreatment,
                             String listOfMedication, DoctorDTO doctor, PatientDTO patient) {
        this.id = id;
        this.diagnostic = diagnostic;
        this.intakeIntervals = intakeIntervals;
        this.periodOfTheTreatment = periodOfTheTreatment;
        this.listOfMedication = listOfMedication;
        this.doctor = doctor;
        this.patient = patient;
    }

    public Integer getId() {
        return id;
    }

    public String getDiagnostic() {
        return diagnostic;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public String getPeriodOfTheTreatment() {
        return periodOfTheTreatment;
    }

    public String getListOfMedication() {
        return listOfMedication;
    }

    @Override
    public String toString() {
        return "MedicationPlanDTO{" +
                "id=" + id +
                ", diagnostic='" + diagnostic + '\'' +
                ", intakeIntervals='" + intakeIntervals + '\'' +
                ", periodOfTheTreatment='" + periodOfTheTreatment + '\'' +
                ", listOfMedication='" + listOfMedication + '\'' +
                '}';
    }
}
