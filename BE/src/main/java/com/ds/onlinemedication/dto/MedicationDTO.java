package com.ds.onlinemedication.dto;

import java.io.Serializable;

public class MedicationDTO implements Serializable {

    private Integer id;
    private String name;
    private String dosage;
    private String listOfSideEffects;

    public MedicationDTO(Integer id, String name, String dosage, String listOfSideEffects) {
        this.id = id;
        this.name = name;
        this.dosage = dosage;
        this.listOfSideEffects = listOfSideEffects;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDosage() {
        return dosage;
    }

    public String getListOfSideEffects() {
        return listOfSideEffects;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "MedicationDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dosage='" + dosage + '\'' +
                ", listOfSideEffects='" + listOfSideEffects + '\'' +
                '}';
    }
}
