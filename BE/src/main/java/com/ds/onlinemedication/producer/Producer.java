package com.ds.onlinemedication.producer;

import com.ds.onlinemedication.dto.PatientDTO;
import com.ds.onlinemedication.entities.Activity;
import com.ds.onlinemedication.service.PatientService;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;

@Component
@Transactional
public class Producer {

    private static final String REGEX = "(\\s{2,})+";
    private static final String PATTERN = "yyyy-MM-dd HH:mm:ss";

    @Autowired
    private AmqpTemplate amqpTemplate;
    @Autowired
    private PatientService patientService;

    @Value("${rabbitmq.queue}")
    private String queueName;
    @Value("${rabbitmq.exchange}")
    private String exchange;
    @Value("${rabbitmq.routingKey}")
    private String routingKey;

    public void produceMessage() {
        SimpleDateFormat sdf = new SimpleDateFormat(PATTERN);
        File file = new File("activity.txt");
        List<Integer> patientIds = getPatientIds();
        Random random = new Random();
        Activity activity;

        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                String[] row = scanner.nextLine().split(REGEX);
                activity = new Activity();

                //patient ID
                int patientId = patientIds.get(random.nextInt(patientIds.size()));
                activity.setPatientId(String.valueOf(patientId));

                //start time
                Date startDate = sdf.parse(row[0]);
                activity.setStartMillis(startDate.getTime());

                //end time
                Date endDate = sdf.parse(row[1]);
                activity.setEndMillis(endDate.getTime());

                //activity
                activity.setActivity(row[2]);

                Thread.sleep(1000);
                amqpTemplate.convertAndSend(exchange, routingKey, activity);
                System.out.println("Send message = " + activity + " - " + System.currentTimeMillis());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<Integer> getPatientIds() {
        return patientService.findAll()
                .stream()
                .map(PatientDTO::getId)
                .collect(Collectors.toList());
    }
}
