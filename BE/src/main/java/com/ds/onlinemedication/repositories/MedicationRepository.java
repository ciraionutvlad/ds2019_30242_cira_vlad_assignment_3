package com.ds.onlinemedication.repositories;

import com.ds.onlinemedication.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface MedicationRepository extends JpaRepository<Medication, Integer> {

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE Medication d SET d.name = :name, d.dosage = :dosage, " +
            "d.listOfSideEffects = :listOfSideEffects  WHERE d.id = :id")
    int updateMedication(@Param("id") int id,
                         @Param("name") String name,
                         @Param("dosage") String dosage,
                         @Param("listOfSideEffects") String listOfSideEffects);
}
