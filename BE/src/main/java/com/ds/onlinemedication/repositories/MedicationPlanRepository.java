package com.ds.onlinemedication.repositories;

import com.ds.onlinemedication.entities.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer>, Serializable {

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE MedicationPlan d SET d.diagnostic = :diagnostic, d.intakeIntervals = :intakeIntervals, " +
            "d.periodOfTheTreatment = :periodOfTheTreatment ,d.listOfMedication = :listOfMedication WHERE d.id = :id")
    int updateMedicationPlan(@Param("id") int id,
                             @Param("diagnostic") String diagnostic,
                             @Param("intakeIntervals") String intakeIntervals,
                             @Param("periodOfTheTreatment") String periodOfTheTreatment,
                             @Param("listOfMedication") String listOfMedication);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("SELECT m FROM MedicationPlan m WHERE m.patient.id = :id")
    List<MedicationPlan> getMedicationPlanByPatientId(@Param("id") Integer id);

}
