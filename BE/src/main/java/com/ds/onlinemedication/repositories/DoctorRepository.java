package com.ds.onlinemedication.repositories;

import com.ds.onlinemedication.entities.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface DoctorRepository extends JpaRepository<Doctor, Integer> {

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE Doctor d SET d.name = :name, d.birthDate = :birthDate, d.gender = :gender, " +
            "d.phone = :phone, d.address = :address WHERE d.id = :id")
    int updateDoctor(@Param("id") int id,
                     @Param("name") String name,
                     @Param("birthDate") String birthDate,
                     @Param("gender") String gender,
                     @Param("phone") String phone,
                     @Param("address") String address);
}
