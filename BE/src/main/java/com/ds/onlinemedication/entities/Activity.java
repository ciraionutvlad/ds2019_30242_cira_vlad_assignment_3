package com.ds.onlinemedication.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Activity implements Serializable {

    @JsonProperty("patient_id")
    private String patientId;
    @JsonProperty("activity")
    private String activity;
    @JsonProperty("start")
    private long startMillis;
    @JsonProperty("end")
    private long endMillis;

    public Activity() {
    }

    public Activity(String patientId, String activity, long startMillis, long endMillis) {
        this.patientId = patientId;
        this.activity = activity;
        this.startMillis = startMillis;
        this.endMillis = endMillis;
    }

    public String getPatientId() {
        return patientId;
    }

    public String getActivity() {
        return activity;
    }

    public long getStartMillis() {
        return startMillis;
    }

    public long getEndMillis() {
        return endMillis;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setStartMillis(long startMillis) {
        this.startMillis = startMillis;
    }

    public void setEndMillis(long endMillis) {
        this.endMillis = endMillis;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "patientId='" + patientId + '\'' +
                ", activity='" + activity + '\'' +
                ", startMillis=" + startMillis +
                ", endMillis=" + endMillis +
                '}';
    }
}
