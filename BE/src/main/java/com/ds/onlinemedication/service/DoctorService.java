package com.ds.onlinemedication.service;

import com.ds.onlinemedication.dto.DoctorBuilder;
import com.ds.onlinemedication.dto.DoctorDTO;
import com.ds.onlinemedication.entities.Doctor;
import com.ds.onlinemedication.repositories.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DoctorService {

    private DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public DoctorDTO findById(Integer id) {
        Optional<Doctor> doctor = doctorRepository.findById(id);

        if (!doctor.isPresent()) {
            throw new ResourceNotFoundException("Doctor: " + id);
        }

        return DoctorBuilder.generateDoctorDTO(doctor.get());
    }

    public List<DoctorDTO> findAll() {
        return doctorRepository.findAll()
                .stream()
                .map(DoctorBuilder::generateDoctorDTO)
                .collect(Collectors.toList());
    }

    public Integer insert(DoctorDTO doctorDTO) {
        return doctorRepository
                .save(DoctorBuilder.generateDoctor(doctorDTO))
                .getId();
    }

    public Integer update(DoctorDTO doctorDTO) {
        Optional<Doctor> doctor = doctorRepository.findById(doctorDTO.getId());

        if (!doctor.isPresent()) {
            throw new ResourceNotFoundException("Doctor" + doctorDTO.getId().toString());
        }

        Doctor newDoctor = DoctorBuilder.generateDoctor(doctorDTO);
        return doctorRepository.updateDoctor(newDoctor.getId(), newDoctor.getName(),
                newDoctor.getBirthDate(), newDoctor.getGender(), newDoctor.getPhone(), newDoctor.getAddress());
    }

    public void delete(int doctorId) {
        doctorRepository.deleteById(doctorId);
    }

}
