package com.ds.onlinemedication.service;

import com.ds.onlinemedication.dto.MedicationPlanDTO;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface MedicationPlanService extends Remote, Serializable {
    MedicationPlanDTO findById(Integer id) throws RemoteException;
    List<MedicationPlanDTO> findAll() throws RemoteException;
    List<MedicationPlanDTO> findAllById(int patientId) throws RemoteException;
    Integer insert(MedicationPlanDTO medicationPlanDTO) throws RemoteException;
    Integer update(MedicationPlanDTO medicationPlanDTO) throws RemoteException;
    void delete(MedicationPlanDTO medicationPlanDTO) throws RemoteException;
}
