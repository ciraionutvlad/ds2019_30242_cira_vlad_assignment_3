package com.ds.onlinemedication.service;

import com.ds.onlinemedication.dto.CaregiverBuilder;
import com.ds.onlinemedication.dto.CaregiverDTO;
import com.ds.onlinemedication.entities.Caregiver;
import com.ds.onlinemedication.repositories.CaregiverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public CaregiverDTO findById(Integer id) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(id);

        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver: " + id);
        }

        return CaregiverBuilder.generateCaregiverDTO(caregiver.get());
    }

    public List<CaregiverDTO> findAll() {
        return caregiverRepository.findAll()
                .stream()
                .map(CaregiverBuilder::generateCaregiverDTO)
                .collect(Collectors.toList());
    }

    public Integer insert(CaregiverDTO caregiverDTO) {
        return caregiverRepository
                .save(CaregiverBuilder.generateCaregiver(caregiverDTO))
                .getId();
    }

    public Integer update(CaregiverDTO caregiverDTO) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverDTO.getId());

        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver" + caregiverDTO.getId().toString());
        }

        Caregiver newCaregiver = CaregiverBuilder.generateCaregiver(caregiverDTO);
        return caregiverRepository.updateCaregiver(newCaregiver.getId(), newCaregiver.getName(),
                newCaregiver.getBirthDate(), newCaregiver.getGender(), newCaregiver.getAddress(),
                newCaregiver.getListOfPatientsTakenCareOf());
    }

    public void delete(int caregiverId) {
        caregiverRepository.deleteById(caregiverId);
    }
}
