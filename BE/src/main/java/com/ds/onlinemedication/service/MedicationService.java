package com.ds.onlinemedication.service;

import com.ds.onlinemedication.dto.MedicationBuilder;
import com.ds.onlinemedication.dto.MedicationDTO;
import com.ds.onlinemedication.entities.Medication;
import com.ds.onlinemedication.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public MedicationDTO findById(Integer id) {
        Optional<Medication> medication = medicationRepository.findById(id);

        if (!medication.isPresent()) {
            throw new ResourceNotFoundException("Medication: " + id);
        }

        return MedicationBuilder.generateMedicationDTO(medication.get());
    }

    public List<MedicationDTO> findAll() {
        return medicationRepository.findAll()
                .stream()
                .map(MedicationBuilder::generateMedicationDTO)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationDTO medicationDTO) {
        return medicationRepository
                .save(MedicationBuilder.generateMedication(medicationDTO))
                .getId();
    }

    public Integer update(MedicationDTO medicationDTO) {
        Optional<Medication> medication = medicationRepository.findById(medicationDTO.getId());

        if (!medication.isPresent()) {
            throw new ResourceNotFoundException("Medication" + medicationDTO.getId().toString());
        }

        Medication newMedication = MedicationBuilder.generateMedication(medicationDTO);
        return medicationRepository.updateMedication(newMedication.getId(), newMedication.getName(),
                newMedication.getDosage(), newMedication.getListOfSideEffects());
    }

    public void delete(int medicationId) {
        medicationRepository.deleteById(medicationId);
    }
}
