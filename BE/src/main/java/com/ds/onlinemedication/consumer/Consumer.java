package com.ds.onlinemedication.consumer;

import com.ds.onlinemedication.entities.Activity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class Consumer implements MessageListener {

    @Override
    @RabbitListener(queues = "${rabbitmq.queue}")
    public void onMessage(Message message) {
        ObjectMapper objectMapper = new ObjectMapper();
        String json = new String(message.getBody());

        try {
            Activity activity = objectMapper.readValue(json, Activity.class);
            System.out.println("Received message = " + activity.toString());

            if (isSleepPeriodLonger(activity)) {
                System.out.println("The sleep period for patient " + activity.getPatientId() + " is longer than 12 hours.");
            }

            if (isLeavingActivityLonger(activity)) {
                System.out.println("The leaving activity (outdoor) for patient" + activity.getPatientId() + " is longer than 12 hours.");
            }

            if (isToiletingLonger(activity)) {
                System.out.println("The period spent in bathroom for patient " + activity.getPatientId() + " is longer than 1 hour.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void containerAckMode(AcknowledgeMode mode) {
        System.out.println("containerAckMode = " + mode);
    }

    private boolean isSleepPeriodLonger(Activity activity) {
        return activity.getActivity().equalsIgnoreCase("Sleeping") && (activity.getEndMillis() - activity.getStartMillis() > 4.32e+7);
    }

    private boolean isLeavingActivityLonger(Activity activity) {
        return activity.getActivity().equalsIgnoreCase("Leaving") && (activity.getEndMillis() - activity.getStartMillis() > 4.32e+7);
    }

    private boolean isToiletingLonger(Activity activity) {
        return activity.getActivity().equalsIgnoreCase("Toileting") && (activity.getEndMillis() - activity.getStartMillis() > 3.6e+6);
    }
}
