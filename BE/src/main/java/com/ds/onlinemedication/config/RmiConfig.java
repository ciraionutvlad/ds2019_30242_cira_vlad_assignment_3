package com.ds.onlinemedication.config;

import com.ds.onlinemedication.repositories.MedicationPlanRepository;
import com.ds.onlinemedication.service.MedicationPlanService;
import com.ds.onlinemedication.service.MedicationPlanServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

@Configuration
public class RmiConfig {
    /*@Bean
    public MedicationPlanServiceImpl createMedicationPlanService() {
        return new MedicationPlanServiceImpl();
    }*/

    @Autowired
    private MedicationPlanRepository medicationPlanRepository;

    private static MedicationPlanServiceImpl service;

    /*@Bean
    public RemoteExporter registerRMIExporter() {
        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceName("PillDispenser");
        exporter.setServiceInterface(MedicationPlanService.class);
        exporter.setService(new MedicationPlanServiceImpl(medicationPlanRepository));
        exporter.setRegistryPort(2099);

        return exporter;
    }*/

    @Bean
    public void register() {
        try {
            service = new MedicationPlanServiceImpl(medicationPlanRepository);
            MedicationPlanService stub = (MedicationPlanService) UnicastRemoteObject.exportObject(service, 0);

            Registry registry = LocateRegistry.createRegistry(1099);
            registry.rebind("PillDispenser", stub);

            //System.out.println("here"+stub.findAll());

            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

}
